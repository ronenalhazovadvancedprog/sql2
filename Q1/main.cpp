#include <string>
#include <iostream>
#include <Windows.h>
#include "sqlite3.h"

using namespace std;

void EpicFail(string message)
{
	cout << message;
	system("pause");
	exit(1);
};

int PeopleSelectCallback(void* idc, int argc, char* argv[], char* idcagain[])
{
	cout << string(argv[0]) << "|" << string(argv[1]) << "\n";
	return 0;
}

void Delete(string path)
{
	remove(path.c_str());
};

bool Exists(string path)
{
	WIN32_FIND_DATAA FindFileData;
	HANDLE handle = FindFirstFileA(path.c_str(), &FindFileData);
	bool res = handle != INVALID_HANDLE_VALUE;
	if (res) { FindClose(handle); }
	return res;
};

int main(int argc, char* argv[])
{
	if (Exists("FirstPart.db"))
	{
		cout << "Previous db detected. Erasing...\n";
		Delete("FirstPart.db");
	}
	sqlite3* db;
	cout << "Opening db..\n";
	if (sqlite3_open("FirstPart.db", &db)) { EpicFail("Unable to open db. "); }
	cout << "DB Opened successfully!\nCreating table... ";
	char* err;
	if (sqlite3_exec(db, "CREATE TABLE people(id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING);", 0, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Inserting Blin... ";
	if (sqlite3_exec(db, "INSERT INTO people(name) VALUES(\"Blin\");", 0, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Inserting Blat... ";
	if (sqlite3_exec(db, "INSERT INTO people(name) VALUES(\"Blat\");", 0, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Inserting Dken... ";
	if (sqlite3_exec(db, "INSERT INTO people(name) VALUES(\"Dken\");", 0, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Now watch.\nid: int|name: string\n";
	if (sqlite3_exec(db, "SELECT * FROM people;", PeopleSelectCallback, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Updating Dken to Blin2... ";
	if (sqlite3_exec(db, "UPDATE people SET name = \"Blin2\" WHERE id = 3;", 0, 0, &err)) { EpicFail(string(err) + " "); }
	cout << "Now watch.\nid: int|name: string\n";
	if (sqlite3_exec(db, "SELECT * FROM people", PeopleSelectCallback, 0, &err)) { EpicFail(string(err) + " "); }
	system("pause");
	sqlite3_close(db);
	return 0;
};