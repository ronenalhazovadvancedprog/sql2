#include <string>
#include <iostream>
#include <Windows.h>
#include "sqlite3.h"
#define RepairDBToggle true /* Define as false to prevent db repair. db repair requires carsDealer.fix file in the project dir. */

using namespace std;

void EpicFail(string message)
{
	cout << message;
	system("pause");
	exit(1);
};

int getIntCallBack(void* idc, int argc, char* argv[], char* idcagain[])
{
	*((int*)idc) = stoi(argv[0]);
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* err)
{
	int price = 0, balance = 0, avaliable = 0, ccars = 0, caccounts = 0;
	if (sqlite3_exec(db, "SELECT COUNT(*) FROM cars", getIntCallBack, &ccars, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, "SELECT COUNT(*) FROM accounts", getIntCallBack, &caccounts, &err)) { EpicFail(string(err) + " "); }
	if (buyerid < 1 || buyerid > caccounts) { return false; }
	if (carid < 1 || carid > ccars) { return false; }
	if (sqlite3_exec(db, (string("SELECT price FROM cars WHERE id = ") + to_string(carid)).c_str(), getIntCallBack, &price, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("SELECT price FROM cars WHERE id = ") + to_string(carid)).c_str(), getIntCallBack, &price, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("SELECT balance FROM accounts WHERE Buyer_id = ") + to_string(buyerid)).c_str(), getIntCallBack, &balance, &err)) { EpicFail(string(err) + " "); }
	if (balance < price) { return false; }
	if (sqlite3_exec(db, (string("SELECT available FROM cars WHERE id = ") + to_string(carid)).c_str(), getIntCallBack, &avaliable, &err)) { EpicFail(string(err) + " "); }
	if (!avaliable) { return false; }
	if (sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("UPDATE accounts SET balance = ") + to_string(balance - price) + string(" WHERE Buyer_id = ") + to_string(buyerid)).c_str(), NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("UPDATE cars SET available = 0 WHERE id = ") + to_string(carid)).c_str(), NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, "COMMIT;", NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	return true;
};

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* err)
{
	int abalance = 0, bbalance = 0, caccounts = 0;
	if (sqlite3_exec(db, "SELECT COUNT(*) FROM accounts", getIntCallBack, &caccounts, &err)) { EpicFail(string(err) + " "); }
	if (from < 1 || from > caccounts) { return false; }
	if (to < 1 || to > caccounts) { return false; }
	if (from == to) { return false; }
	if (sqlite3_exec(db, (string("SELECT balance FROM accounts WHERE Buyer_id = ") + to_string(from)).c_str(), getIntCallBack, &abalance, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("SELECT balance FROM accounts WHERE Buyer_id = ") + to_string(to)).c_str(), getIntCallBack, &bbalance, &err)) { EpicFail(string(err) + " "); }
	if (abalance < amount) { return false; }
	if (sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("UPDATE accounts SET balance = ") + to_string(abalance - amount) + string(" WHERE Buyer_id = ") + to_string(from)).c_str(), NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, (string("UPDATE accounts SET balance = ") + to_string(bbalance + amount) + string(" WHERE Buyer_id = ") + to_string(to)).c_str(), NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	if (sqlite3_exec(db, "COMMIT;", NULL, NULL, &err)) { EpicFail(string(err) + " "); }
	return true;
};

int main(int argc, char* argv[])
{
	if (RepairDBToggle) { CopyFileA("carsDealer.fix", "carsDealer.db", false); }
	sqlite3* db;
	char* err = new char[100] { 0 };
	cout << "Opening db..\n";
	if (sqlite3_open("carsDealer.db", &db)) { EpicFail("Unable to open db. "); }
	cout << "DB Opened successfully!\n";
	//cout << "Transfering from 2 to 1.." << balanceTransfer(2, 1, 100, db, err) << "\n";
	cout << "Trying once.. " << carPurchase(12, 3, db, err) << "\n";
	cout << "Trying twice.. " << carPurchase(1, 1, db, err) << "\n";
	cout << "Trying thrice.. " << carPurchase(1, 22, db, err) << "\n";
	system("pause");
	sqlite3_close(db);
	return 0;
};